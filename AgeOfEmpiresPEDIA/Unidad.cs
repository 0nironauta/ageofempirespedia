﻿namespace AgeOfEmpiresPEDIA
{
	/// <summary>
	/// Clase del modelo que define una Unidad de la API
	/// TO DO -> Añadir el resto de campos
	/// </summary>
	class Unidad
    {
		/// <summary>
		/// Identificador de la unidad
		/// </summary>
		public string id;

		/// <summary>
		/// Nombre de la unidad
		/// </summary>
		public string name;

		/// <summary>
		/// Descripción de la unidad
		/// </summary>
		public string description;

		/// <summary>
		/// Nombre de la expansión
		/// </summary>
		public string expansion;

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="nuevoID">ID de la unidad</param>
		/// <param name="nombre">Nombre de la unidad</param>
		/// <param name="descripcion">Descripción de la unidad</param>
		/// <param name="exp">Expansión en la que aparece la unidad</param>
		public Unidad(string nuevoID, 
					  string nombre, 
					  string descripcion, 
					  string exp)
		{
			id = nuevoID;
			name = nombre;
			description = descripcion;
			expansion = exp;

		}

	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;

namespace AgeOfEmpiresPEDIA
{
	/// <summary>
	/// Gestiona la conexión HHTP
	/// </summary>
    class Conexion
    {
		/// <summary>
		/// URL base de conexión
		/// </summary>
        private string URL;

		/// <summary>
		/// Cliente HTTP
		/// </summary>
        HttpClient cliente;

		/// <summary>
		/// Respuesta recibida
		/// </summary>
        HttpResponseMessage respuesta;

		/// <summary>
		/// Contructor
		/// </summary>
		/// <param name="url">URL base de la conexión</param>
		public Conexion(string url)
        {
            URL = url;
        }

		/// <summary>
		/// Realiza la conexión con la URL establecida.
		/// </summary>
        public void Conectar()
        {
            cliente = new HttpClient();
            cliente.BaseAddress = new Uri(URL);
            cliente.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

		/// <summary>
		/// Hace una llamada directa a una URL recibida por parámetro
		/// Usada para descargar datos puntuales
		/// </summary>
		/// <param name="URLCompleta">URL completa de conexión</param>
		/// <returns>Devuelve el string de la respuesta o "error" si ha habido algún problema.</returns>
		public string LlamadaDirecta(string URLCompleta)
		{
			cliente.BaseAddress = new Uri(URLCompleta);
			HttpResponseMessage respuesta = cliente.GetAsync("").Result;  // Blocking call! Program will wait here until a response is received or a timeout occurs.
			if (respuesta.IsSuccessStatusCode)
			{
				return respuesta.Content.ReadAsStringAsync().Result;
			}
			else
			{
				Console.WriteLine("{0} ({1})", (int)respuesta.StatusCode, respuesta.ReasonPhrase);
				return "error";
			}
		}

		/// <summary>
		/// Hace una llamada al apartado de la API especificado en "parametros" y devuelve el resultado
		/// </summary>
		/// <param name="parametros">Apartado de la API para concatenar a la dirección base</param>
		/// <returns>Devuelve un string con la respuesta o "error" si ha habido algún problema</returns>
        public string LeerDatos(string parametros)
        {
            HttpResponseMessage respuesta = cliente.GetAsync(parametros).Result;  // Blocking call! Program will wait here until a response is received or a timeout occurs.
            if (respuesta.IsSuccessStatusCode)
            {
                return respuesta.Content.ReadAsStringAsync().Result;
            }
            else
            {
                Console.WriteLine("{0} ({1})", (int)respuesta.StatusCode, respuesta.ReasonPhrase);
                return "error";
            }
        }
    }


}

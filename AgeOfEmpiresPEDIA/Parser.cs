﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgeOfEmpiresPEDIA
{
	/// <summary>
	/// Parseador Newstonsoft para parsear JSON y otras utilidades de parseo de strings
	/// TO DO -> Separar en 2 clases (una para JSON y otra para las utilidades de parseo de strings)
	/// </summary>
    class Parser
    {
		/// <summary>
		/// Deserializa el contenido recibido por parámetro
		/// </summary>
		/// <param name="datos">Un string con una cadena JSON</param>
		/// <returns>Devuelve los datos JSON parseados</returns>
        public dynamic Deserializar(string datos)
        {
            return JsonConvert.DeserializeObject<dynamic>(datos);
        }

		/// <summary>
		/// Función específica para tratar datos de la API.
		/// Recorta el string de unidad única.
		/// </summary>
		/// <param name="original">Cadena completa</param>
		/// <param name="indice1">Inicio del segmento deseado</param>
		/// <param name="indice2">Fin del segmento deseado</param>
		/// <returns>Devuelve la unidad extraída de la cadena total</returns>
		public static string RecortarStringUnidad(string original, string indice1, string indice2)
		{
			int i1 = original.LastIndexOf(indice1);
			int i2 = original.LastIndexOf(indice2);
			int cantidad = i2 - i1;
			string resultado = original.Substring(i1 + 1, cantidad - 1);

			return resultado;
		}

		/// <summary>
		/// Función específica para tratar datos de la API.
		/// Recorta el string de URL.
		/// </summary>
		/// <param name="original">Cadena completa</param>
		/// <param name="indice1">Inicio del segmento deseado</param>
		/// <param name="indice2">Fin del segmento deseado</param>
		/// <returns>Devuelve la URL extraída de la cadena total</returns>
		public static string RecortarStringURL(string original, string indice1, string indice2)
		{
			int i1 = original.IndexOf(indice1);
			int i2 = original.LastIndexOf(indice2);
			int cantidad = i2 - i1;
			string resultado = original.Substring(i1 + 1, cantidad - 1);

			return resultado;
		}

		/// <summary>
		/// Capitaliza la primera letra del string recibido
		/// </summary>
		/// <param name="s">Cadena a capitalizar</param>
		/// <returns>Cadena modificada</returns>
		public static string UppercaseFirst(string s)
		{
			if (string.IsNullOrEmpty(s))
			{
				return string.Empty;
			}
			return char.ToUpper(s[0]) + s.Substring(1);
		}
	}
}

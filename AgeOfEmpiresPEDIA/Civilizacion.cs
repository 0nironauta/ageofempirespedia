﻿namespace AgeOfEmpiresPEDIA
{
	/// <summary>
	/// Clase del modelo que define una Civilización de la API
	/// TO DO -> Añadir el resto de campos
	/// </summary>
    class Civilizacion
    {
		/// <summary>
		/// Identificador de la civilización
		/// </summary>
        public string id;

		/// <summary>
		/// Nombre de la civilización
		/// </summary>
        public string name;

		/// <summary>
		/// Nombre de la expansión
		/// </summary>
        public string expansion;

		/// <summary>
		/// Tipo de ejército
		/// </summary>
        public string armyType;

		/// <summary>
		/// Unidad única de esta civilización
		/// </summary>
        public string uniqueUnit;

		/// <summary>
		/// Tecnología única de esta civilización
		/// </summary>
        public string uniqueTech;

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="nuevoID">ID de la civilización</param>
		/// <param name="nombre">Nombre de la civilización</param>
		/// <param name="exp">Expansión en la que aparece</param>
		/// <param name="tipoEjercito">Tipo de ejército</param>
		/// <param name="unidadUnica">Unidad única</param>
		/// <param name="tecnologiaUnica">Tecnología única</param>
		public Civilizacion(string nuevoID, 
							string nombre, 
							string exp, 
							string tipoEjercito, 
							string unidadUnica, 
							string tecnologiaUnica)
        {
            id = nuevoID;
            name = nombre;
            expansion = exp;
            armyType = tipoEjercito;
            uniqueUnit = unidadUnica;
            uniqueTech = tecnologiaUnica;
        }
    }
}

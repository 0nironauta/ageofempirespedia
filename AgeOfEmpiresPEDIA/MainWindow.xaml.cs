﻿/*
 *  AGE OF EMPIRES PEDIA
 *  An explorer of the Age of Empires II API
    Copyright(C) 2020  Jorge García Colmenar (@JorgeMcLoud)

    This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.If not, see<https://www.gnu.org/licenses/>.

	AVISO: Este proyecto es educativo y no está completo.
	El uso del español es intencional para distinguir el código del lenguaje
	del código explicativo.
	En las clases Civilizacion.cs y Unidad.cs, los campos están en inglés
	para facilitar la interpretación de la API.
	 */

// MainWindow.xaml.cs
// Controlador de la aplicación

using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace AgeOfEmpiresPEDIA
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
		#region MIEMBROS_DE_LA_CLASE
		/// <summary>
		/// URL base de la API
		/// </summary>
		private const string URL = "https://age-of-empires-2-api.herokuapp.com/api/v1/";

		/// <summary>
		/// Lista de civilizaciones
		/// </summary>
        List<Civilizacion> civilizaciones = new List<Civilizacion>();

		/// <summary>
		/// Lista de unidades
		/// </summary>
        List<Unidad> unidades = new List<Unidad>();

		/// <summary>
		/// Lista de estructuras
		/// </summary>
        List<Estructura> estructuras = new List<Estructura>();

		/// <summary>
		/// Es el listado que se está mostrando en la interfaz de usuario
		/// </summary>
		string listadoActual;
		#endregion

		#region METODOS_DE_LA_CLASE
		/// <summary>
		/// Método que controla la interfaz de usuario
		/// </summary>
		public MainWindow() 
        {
            InitializeComponent();
            ObtenerDatosAPI();
            RellenarCivilizaciones();
			listadoActual = "Civilizaciones";
		}

		/// <summary>
		/// Realiza la conexión y descarga los datos de la API
		/// </summary>
        void ObtenerDatosAPI()
        {
            Conexion conexion = new Conexion(URL);
            conexion.Conectar();
            ObtenerCivilizaciones(conexion);
            ObtenerUnidades(conexion);
            ObtenerEstructuras(conexion);
        }

		/// <summary>
		/// Conecta con la API de civilizaciones y rellena la lista
		/// </summary>
		/// <param name="conexion">La conexión HTTP establecida</param>
        void ObtenerCivilizaciones(Conexion conexion)
        {
            string datos = conexion.LeerDatos("civilizations");
            Parser parser = new Parser();
            dynamic datosParseados = parser.Deserializar(datos);

            foreach (var civ in datosParseados.civilizations)
            {
                Civilizacion civi = new Civilizacion(civ.id.ToString(), 
                                                     civ.name.ToString(),
                                                     civ.expansion.ToString(),
                                                     civ.army_type.ToString(),
                                                     civ.unique_unit.ToString(),
                                                     civ.unique_tech.ToString());
                civilizaciones.Add(civi);
            }
        }

		/// <summary>
		/// Conecta con la API de unidades y rellena la lista
		/// </summary>
		/// <param name="conexion">La conexión HTTP establecida</param>
		void ObtenerUnidades(Conexion conexion)
        {
			string datos = conexion.LeerDatos("units");
			Parser parser = new Parser();
			dynamic datosParseados = parser.Deserializar(datos);

			foreach (var uni in datosParseados.units)
			{
				Unidad unidad = new Unidad(uni.id.ToString(),
										   uni.name.ToString(),
										   uni.description.ToString(),
										   uni.expansion.ToString()
													 );
				unidades.Add(unidad);
			}
		}

		/// <summary>
		/// TO DO: Conecta con la API de unidades y rellena la lista
		/// </summary>
		/// <param name="conexion">La conexión HTTP establecida</param>
		void ObtenerEstructuras(Conexion conexion)
        {
			// Not developed yet.
			throw new NotImplementedException();
		}

		/// <summary>
		/// Función llamada desde la UI. Cambia el listado a mostrar.
		/// </summary>
		/// <param name="sender">El botón pulsado</param>
		/// <param name="e">Los argumentos del evento (ninguno)</param>
		void CambiarListado(object sender, RoutedEventArgs e)
		{
			Button bt = (Button)sender;
			string tag = bt.Tag.ToString();

			if (tag != listadoActual)
			{
				Listado.Children.Clear();

				listadoActual = tag;

				switch (tag)
				{
					case "Civilizaciones":
						RellenarCivilizaciones();
						break;

					case "Unidades":
						RellenarUnidades();
						break;

					case "Estructuras":

						break;
				}
			}
		}

		/// <summary>
		/// Rellena el panel izquierdo con la lista de civilizaciones, creando un botón por cada
		/// elemento de la lista de civilizaciones. A continuación muestra en el panel derecho
		/// la primera civilización.
		/// </summary>
        void RellenarCivilizaciones()
        {
            for (int i= 0; i<civilizaciones.Count;i++)
            {
				Civilizacion civ = civilizaciones[i];

                Button bt = new Button();
                bt.Tag = civ.id;
                bt.Content = civ.name;
                bt.Height = 50;
				bt.Click += EventoMostrarCivilizacion;

				Style style = FindResource("BotonListado") as Style;
				bt.Style = style;

				Listado.Children.Add(bt);         
            }
            MostrarCivilizacion(1);
        }


		/// <summary>
		/// Rellena el panel izquierdo con la lista de unidades, creando un botón por cada
		/// elemento de la lista de unidades. A continuación muestra en el panel derecho
		/// la primera unidad.
		/// </summary>
		void RellenarUnidades()
		{
			for (int i = 0; i < unidades.Count; i++)
			{
				Unidad unidad = unidades[i];

				Button bt = new Button();
				bt.Tag = unidad.id;
				bt.Content = unidad.name;
				bt.Height = 50;
				bt.Click += EventoMostrarUnidad;

				Style style = FindResource("BotonListado") as Style;
				bt.Style = style;

				Listado.Children.Add(bt);
			}
			MostrarUnidad(1);
		}

		/// <summary>
		/// Función llamada desde los botones del listado de civilizaciones.
		/// Obtiene el id de la civilización y se lo pasa a la función MostrarCivilizacion
		/// </summary>
		/// <param name="sender">El botón pulsado</param>
		/// <param name="e">No se recibe nada</param>
		void EventoMostrarCivilizacion(object sender, RoutedEventArgs e)
        {
			Button bt = (Button)sender;
			string tag = bt.Tag.ToString();
			int id = int.Parse(tag);

			MostrarCivilizacion(id);
		}


		/// <summary>
		/// Función llamada desde los botones del listado de unidades.
		/// Obtiene el id de la unidad y se lo pasa a la función MostrarUnidad
		/// </summary>
		/// <param name="sender">El botón pulsado</param>
		/// <param name="e">No se recibe nada</param>
		void EventoMostrarUnidad(object sender, RoutedEventArgs e)
		{
			Button bt = (Button)sender;
			string tag = bt.Tag.ToString();
			int id = int.Parse(tag);

			MostrarUnidad(id);
		}

		/// <summary>
		/// Muestra en el panel derecho los datos de la civilización cuyo ID recibe por parámetro
		/// </summary>
		/// <param name="id">ID de la civilización para mostrar</param>
		void MostrarCivilizacion(int id)
		{
			ListadoDatos.Children.RemoveRange(2, ListadoDatos.Children.Count);

			Civilizacion civ = civilizaciones[id-1];
			name.Text = civ.name;
			expansion.Text = civ.expansion;

			CrearCampo("Army Type", civ.armyType);

			CrearCampoInteractivo("Unique Unit", civ.uniqueUnit);

			CrearCampoInteractivo("Unique Technology", civ.uniqueTech);
		}

		/// <summary>
		/// Muestra en el panel derecho los datos de la unidad cuyo ID recibe por parámetro
		/// </summary>
		/// <param name="id">ID de la unidad para mostrar</param>
		void MostrarUnidad(int id)
		{
			ListadoDatos.Children.RemoveRange(2, ListadoDatos.Children.Count);

			Unidad unidad = unidades[id - 1];
			name.Text = unidad.name;
			expansion.Text = unidad.expansion;

			CrearCampo("Description", unidad.description);
		}

		/// <summary>
		/// Crea un campo no interactivo en el panel derecho con la etiqueta y el valor recibidos por parámetro.
		/// </summary>
		/// <param name="etiqueta">Etiqueta del campo (label xaml)</param>
		/// <param name="valor">Valor del campo</param>
		void CrearCampo(string etiqueta, string valor)
		{
			StackPanel wpDescripcion = new StackPanel();
			Label tbDescripcionLabel = new Label();
			TextBlock tbDescripcionValue = new TextBlock();

			Style style = FindResource("StackPanelsDerecha") as Style;
			wpDescripcion.Style = style;

			tbDescripcionLabel.Content = etiqueta;
			tbDescripcionValue.Text = valor;

			wpDescripcion.Children.Add(tbDescripcionLabel);
			wpDescripcion.Children.Add(tbDescripcionValue);
			ListadoDatos.Children.Add(wpDescripcion);
		}

		/// <summary>
		/// Crea un campo interactivo en el panel derecho con la etiqueta y el valor recibidos por parámetro.
		/// El valor del campo será clicable.
		/// </summary>
		/// <param name="etiqueta">Etiqueta del campo (label xaml)</param>
		/// <param name="valor">Valor del campo</param>
		void CrearCampoInteractivo(string etiqueta, string valor)
		{
			StackPanel wp = new StackPanel();
			Label tbLabel = new Label();
			Button tbValue = new Button();

			Style style_sp = FindResource("StackPanelsDerecha") as Style;
			wp.Style = style_sp;

			tbLabel.Content = etiqueta;

			tbValue.Content = Parser.UppercaseFirst(Parser.RecortarStringUnidad(valor, "/", "\""));

			tbValue.Tag = valor;
			Style style = FindResource("CampoInteractivo") as Style;
			tbValue.Style = style;
			tbValue.Click += DescargarDatos;

			wp.Children.Add(tbLabel);
			wp.Children.Add(tbValue);
			ListadoDatos.Children.Add(wp);
		}

		/// <summary>
		/// Descarga datos extra de la unidad seleccionada en un campo interactivo
		/// </summary>
		/// <param name="sender">Botón pulsado (la unidad seleccionada para mostrar)</param>
		/// <param name="e">No se recibe nada</param>
		void DescargarDatos(object sender, RoutedEventArgs e)
		{
			Button bt = (Button)sender;
			string tag = bt.Tag.ToString();

			string URLUnidad = Parser.RecortarStringURL(tag, "\"", "\"");

			Conexion conexion = new Conexion(URL);
			conexion.Conectar();
			string resultado = conexion.LlamadaDirecta(URLUnidad);
			if (resultado == "error")
			{
				// TO DO -> Gestionar el error
			}

			Parser parser = new Parser();
			dynamic datosParseados = parser.Deserializar(resultado);

			Listado.Children.Clear();
			listadoActual = "Unidades";
			RellenarUnidades();

			MostrarUnidad((int)datosParseados.id);
		}
		#endregion
	}
}
